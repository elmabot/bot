module.exports = {
  apps: [
    {
      autorestart: true,
      name: "Elma",
      script: "dist/bot.js",
      instances: 1,
      watch_delay: 3000,
      watch: "dist",
      max_memory_restart: "512M",
      output: "log/pm2.out.log",
      error: "log/pm2.error.log",
    },
  ],
};
