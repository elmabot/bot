// eslint-disable-next-line @typescript-eslint/no-var-requires
require("source-map-support").install();

import * as Discord from "discord.js";
import * as Path from "path";
import { Commander, config, logger, Settings } from "./base/";
import { getPrefixRegex } from "./util/";

/**
 * The main script where the bot executes, this should be run as the main file
 */

/**
 * a global-ish variable to get the root of the app in order to locate files more effectively
 */
export const BASEDIR = Path.join(__dirname, "../");

/**
 * Retrieves the bot's current version from the package.json
 */
// eslint-disable-next-line @typescript-eslint/no-var-requires
export const VERSION = require(Path.join(__dirname, "../package.json")).version;

/**
 * Instantiates the connection to Discord
 */
export const discordClient = new Discord.Client();

logger.info("#Bot started");

/**
 * The [[Settings]] singleton
 */
export let settings: Settings;
/**
 * The [[Commander]] singleton which will be used to call all commands
 */
export const commands = new Commander();

/**
 * A local variable to determine whether the bot is ready to parse commands
 */
let READY = false;

/**
 * async function which instantiates the necessary global [[Commander]] and [[Settings]] objects
 * @param client {Client} - the Discordjs client object
 * @return {Promise<boolean>} - the value of this will be set to the [[READY]] variable
 */
async function startupSequence(client: Discord.Client): Promise<boolean> {
  let STARTUPREADY = false;

  await client.user.setPresence({
    afk: true,
    activity: { name: "STARTUP SEQUENCE", type: "WATCHING" },
    status: "invisible",
  });

  settings = new Settings(client, config.discord.ownerid);
  await settings
    .init()
    .then(() => {
      STARTUPREADY = true;
    })
    .catch(() => {
      STARTUPREADY = false;
      logger.error("No guilds were found by Settings object");
      client.user.setPresence({
        afk: true,
        activity: { name: "The error log", type: "WATCHING" },
        status: "dnd",
      });
    });

  await commands
    .register()
    .then(() => {
      STARTUPREADY = true;
      client.user.setPresence({
        afk: false,
        activity: { name: "To the server", type: "LISTENING" },
        status: "online",
      });
    })
    .catch(() => {
      STARTUPREADY = false;
      client.user.setPresence({
        afk: true,
        activity: { name: "The error log", type: "WATCHING" },
        status: "dnd",
      });
    });

  return STARTUPREADY;
}

// Where the discord.js magic really starts
discordClient
  .login(config.discord.token)
  .then(() => logger.info("##Login successful"))
  .catch((err: Error) =>
    logger.error("Error while attempting login to Discord\n###" + err.stack)
  );

discordClient.on("ready", () => {
  logger.info("##Bot is ready");
  // Once the bot is started the settings and commander singletons are instantiated
  startupSequence(discordClient).then((response) => {
    READY = response;
    logger.info("###Settings and Commands initialized");
  });
});

// Some simple even listeners which record and register Discord.js warnings and errors
discordClient.on("warn", (info: string) => logger.warn(info));
if (process.env.NODE_ENV == "development") {
  discordClient.on("debug", (info: string) => logger.debug(info));
}
discordClient.on("error", (error: Error) => logger.error(error.stack));

// The actual message event listener
discordClient.on("message", (message: Discord.Message) => {
  // If Settings or commands aren't ready yet messages are ignored
  if (!READY) {
    return;
  }

  // No talkie to other bots or in private dms
  if (message.author.bot || message.channel.type === "dm") {
    return;
  }

  // If it's a command it runs and exits the thing
  const commandMatch = message.content.match(getPrefixRegex(message.guild.id));
  if (
    commandMatch !== null &&
    commandMatch.length > 1 &&
    settings.ready === true
  ) {
    commands.run(commandMatch[1], message);
    return;
  }

  // if it's a markov it runs and exits
  // const markovComm = message.content.match(
  //   new RegExp(
  //     "^(<@!?" +
  //       discordClient.user.id +
  //       ">|" +
  //       config.global.name +
  //       ") (do|are) you (.+)$",
  //     "mi"
  //   )
  // );
  // if (markovComm !== null && markovComm.length > 2) {
  //   commands.run("markov", message);
  //   return;
  // }

  // if it's a question is automatically defaults to the eightball function
  if (
    message.content.match(
      new RegExp(
        "^(<@!?" +
          discordClient.user.id +
          ">|" +
          config.global.name +
          ") .+\\?$",
        "mi"
      )
    ) !== null
  ) {
    commands.run("eightball", message);
    return;
  }

  // if it's a URL else it can try and see if it's an article to be summarized
  if (message.content.match(new RegExp("^https?:\\/\\/", "m")) !== null) {
    return;
  }
});
