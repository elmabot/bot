import * as fs from "fs";
import * as Path from "path";
import { Command } from "../../base";
import { BASEDIR } from "../../bot";

/**
 * A simple interface describing the structure of the `eightball.json` file
 */
interface Iresponses {
  standalone: string[];
  custom: {
    begin: string[];
    end: string[];
  };
}
/**
 * Returns a simple eightball response from a set of templates
 * The templates should exist in the 'res' folder as `eightball.json`
 */
export class Eightball extends Command {
  protected desc = "Gazes into to beyond to bring a vision of the future";

  private readonly responses: Iresponses | boolean;

  constructor() {
    super();
    this.responses = this.getResponses();
    this.commandName = "eightball";
    this.needParamValues = false;
  }

  protected exec(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      if (!this.responses) {
        reject(false);
      }

      const responses = this.responses as Iresponses;
      const chance = Math.round(Math.random());
      if (chance === 1) {
        const begin = Math.floor(
          Math.random() * (responses.custom.begin.length - 1) + 1
        );
        const end = Math.floor(
          Math.random() * (responses.custom.end.length - 1) + 1
        );
        this.reply =
          "🎱`" +
          responses.custom.begin[begin] +
          " " +
          responses.custom.end[end] +
          "`🎱";
        resolve(true);
      } else {
        const num = Math.floor(
          Math.random() * (responses.standalone.length - 1) + 1
        );
        this.reply = "🎱`" + responses.standalone[num] + "`🎱";
        resolve(true);
      }
    });
  }

  /**
   * parses the `eightball.json` file and returns the templates within as an object
   * @return {Iresponses|boolean} object containing the templates in a series of arrays
   */
  private getResponses(): Iresponses | boolean {
    try {
      return JSON.parse(
        fs.readFileSync(Path.join(BASEDIR, "/res/eightball.json"), "utf8")
      );
    } catch (err) {
      this.error = err;
      this.errorString = "error while trying to open file for eightball";
      return false;
    }
  }
}
