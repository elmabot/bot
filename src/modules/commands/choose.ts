import { Command } from "../../base";
import { getPrefix } from "../../util/";

export class Choose extends Command {
  protected desc =
    "Choose something from multiple options seperated by commas or semi-colons";

  constructor() {
    super();
    this.commandName = "choose";
    this.params = "<option1, option2; option3; ...>";
  }

  public exec(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      let bits;
      try {
        bits = this.message.content.match(
          new RegExp("^(" + getPrefix(this.server) + "\\w+) (.+)", "i")
        );
      } catch (e) {
        reject(e);
      }

      if (bits === null) {
        this.reply = "I need options to pick from 😖";
        resolve(true);
      } else {
        const theString = bits[2];
        let list = theString.split(new RegExp("[,;]|(or)", "m"));
        list = list.filter((v: string) => v !== "" && v !== "or");
        if (list.length < 1) {
          this.reply = "I can't choose from empty options 😖";
        } else if (list.length === 1) {
          this.reply =
            "You only gave me one option so I'm gonna say " + list[0];
        } else {
          this.reply = list[Math.floor(Math.random() * list.length)];
        }
        resolve(true);
      }
    });
  }
}
