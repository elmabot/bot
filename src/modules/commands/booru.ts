import { Command } from "../../base";

export class Booru extends Command {
  protected desc = "search an image from either danbooru or safebooru";

  constructor() {
    super();
    this.commandName = "booru";
    this.params = "<tags seperated by commas>";
  }

  public exec(): Promise<boolean> {
    return new Promise((resolve) => {
      resolve(true);
    });
  }
}
