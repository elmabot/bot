import { Reddit } from "./reddit";

export class Swift extends Reddit {
  protected desc = "Fetch me some Taytay";

  constructor() {
    super();
    this.commandName = "swift";
    this.needParamValues = false;
    this.aliases = "tay";
    this.params = "";
    this.nsfw = false;
  }

  public exec(): Promise<boolean> {
    return this.fetchRed("TaylorSwiftPictures");
  }
}
