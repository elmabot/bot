import * as fs from "fs";
import * as Path from "path";
import { Command } from "../../base";
import { BASEDIR } from "../../bot";

export class Sway extends Command {
  protected desc = "Summons an appropriate visual representation";

  constructor() {
    super();
    this.commandName = "sway";
    this.needParamValues = false;
  }

  public exec(): Promise<boolean | Error> {
    return new Promise((resolve, reject) => {
      if (fs.existsSync(Path.join(BASEDIR, "/res/img/sway.jpg"))) {
        this.reply = {
          files: [
            {
              attachment: Path.join(BASEDIR, "/res/img/sway.jpg"),
              name: "sway.jpg",
            },
          ],
        };
        resolve(true);
      } else {
        this.errorString = "Sway image not found";
        this.error = new Error("Sway image not found");
        reject(false);
      }
    });
  }
}
