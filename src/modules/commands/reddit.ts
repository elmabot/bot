import { TextChannel } from "discord.js";
import { Command } from "../../base";
import { VERSION } from "../../base/config";
import { fetchToJSON } from "../../util/fetchHelper";

/**
 * Queries a specified subreddit for a random image post to post in the chat
 */
export class Reddit extends Command {
  protected commandName = "reddit";
  protected desc =
    "gets a random post from a specified subreddit, only returns SFW posts unless the channels is" +
    "marked NSFW";
  protected params = "<subreddit name>";
  protected aliases = "r";
  /**
   * Array of strings of subreddits the bot should not pull from
   */
  private forbidden: string[] = [
    "watchpeopledie",
    "kotakuinaction",
    "physicalremoval",
    "braincels",
    "redpill",
    "drama",
    "gamerghazi",
    "popping",
    "poop",
    "popping",
    "shit",
    "2booty",
    "gore",
    "guro",
    "shit",
    "cringeanarchy",
    "shittyfoodporn",
  ];
  /**
   * image hosting domains accepted by Reddit to prevent linking to unkown sites
   */
  private domains: string[] = ["i.redd.it", "i.imgur.com", "gfycat.com"];
  /**
   * Whether or not the channel for the command is NSFW
   */
  protected nsfw: boolean;
  /**
   * Stores the URL of a suitable post
   */
  private ResponseUrl: string;

  private httpOptions = {
    method: "GET",
    headers: {
      "User-Agent": `nodejs:elmabot:${VERSION} (by clericalterrors)`,
    },
  };

  constructor() {
    super();
  }

  public exec(): Promise<boolean> {
    this.nsfw = this.returnNSFW();
    return this.fetchRed(this.paramValues[0]);
  }

  /**
   * checks whether the channel the request was made in was NSFW or not
   */
  private returnNSFW(): boolean {
    const chan = this.message.channel as TextChannel;
    return chan.nsfw;
  }

  private fetchFromReddit(
    uri: string
  ): Promise<
    IRedditAboutResponse | IRedditErrorResponse | Array<IRedditRandomResponse>
  > {
    const resp = fetchToJSON(uri, this.httpOptions) as Promise<
      IRedditAboutResponse | IRedditErrorResponse | Array<IRedditRandomResponse>
    >;
    return resp;
  }

  private fetchFromRedditAbout(sub: string): Promise<IRedditAboutResponse> {
    const uri = `https://api.reddit.com/r/${sub}/about`;
    return this.fetchFromReddit(uri) as Promise<IRedditAboutResponse>;
  }

  private fetchFromRedditRandom(
    sub: string
  ): Promise<Array<IRedditRandomResponse>> {
    const ext = this.nsfw ? "" : "?obey_over18=true";
    const uri = `https://api.reddit.com/r/${sub}/random.json${ext}`;
    return this.fetchFromReddit(uri) as Promise<Array<IRedditRandomResponse>>;
  }

  /**
   * checks the subreddit if it NSFW or not and whether it's quarantined or not
   * queries a random post and returns an appropriate one if able
   * @param sub {string} the name of the subreddit to query
   */
  protected async fetchRed(sub: string): Promise<boolean> {
    if (this.forbidden.includes(sub)) {
      this.reply = "I'm not allowed to pull anything from that subreddit";
      return true;
    }
    const check = await this.checkSubreddit(sub);
    if (!check) {
      return true;
    }
    this.message.channel.startTyping();
    const response = await this.queryReddit(sub);
    this.message.channel.stopTyping(true);
    if (!response) {
      this.reply = "I'm sorry, but I couldn't find any suitable posts";
    } else {
      this.reply = this.ResponseUrl;
    }
    return true;
  }

  /**
   * Performs the query to get a post from the subreddit, or empty if a suitable post can't be found
   * @param sub {string} the name of the subreddit
   */
  private async queryReddit(sub: string): Promise<boolean> {
    let found = false;
    for (let limit = 0; limit < 5 && !found; limit++) {
      const redditResponse = await this.fetchFromRedditRandom(sub);
      found = this.checkResponseChildren(redditResponse[0]);
    }
    return found;
  }

  private redditErrorReplies(reason: string): void {
    switch (reason) {
      case "private":
        this.reply = "I'm sorry, but it seems like the subreddit is private";
        break;
      case "banned":
        this.reply = "I'm sorry, it appeares this subreddit was banned";
        break;
      case "quarantine":
        // eslint-disable-next-line prettier/prettier
        this.reply =
          "I'm sorry, this subreddit appears to have been quarantined";
        break;
      case "quarantined":
        // eslint-disable-next-line prettier/prettier
        this.reply =
          "I'm sorry, this subreddit appears to have been quarantined";
        break;
      case "over18":
        this.reply =
          "I'm sorry I can't return posts from this subreddit as it is an NSFW subreddit";
        break;
      case "doesnotexist":
        this.reply = "I'm sorry, I can't find that subreddit";
        break;
    }
  }

  /**
   * Queries a subreddit's _About_ info to see if it's NSFW and whether it's quarantined or private or not
   * @param sub {string} the name of the subreddit
   * @returns {Promise<boolean|string>} `true` if all ok, false if NSFW even thought the channel isn't or a `string`
   * containing `banned`, `private`, or `quarantined`, whichever of those is true for the sub
   */
  private async checkSubreddit(sub: string): Promise<boolean> {
    const response = await this.fetchFromRedditAbout(sub).catch(() => {
      return false;
    });
    if (!response) {
      return false;
    }
    const checkStat = this.checkResponseStuff(response);
    if (checkStat !== false) {
      this.redditErrorReplies(checkStat as string);
      return false;
    }
    return true;
  }

  private checkResponseStuff(response: unknown): string | boolean {
    let resp:
      | IRedditAboutResponse
      | IRedditErrorResponse
      | IRedditRandomResponse;
    resp = response as IRedditErrorResponse;
    if (resp.reason !== undefined) {
      return resp.reason;
    }
    resp = response as IRedditAboutResponse;
    if (resp.data !== undefined) {
      if (resp.data.quarantine) {
        return "quarantine";
      } else if (resp.data.over18 && !this.nsfw) {
        return "over18";
      }
      resp = response as IRedditRandomResponse;
      if (resp.data.children !== undefined) {
        return "doesnotexist";
      }
    }
    return false;
  }

  /**
   * Loops through all children of a query response to find if any of them meet the required critera
   * @param Response {array} the array of the `children` part of the Reddit API response
   * @returns {boolean} `true` if a suitable post is found `false` otherwise
   */
  private checkResponseChildren(Response: IRedditRandomResponse): boolean {
    const Children = Response.data.children;
    let result = false;
    for (const Child of Children) {
      const data = Child.data;

      if (
        ((data.over_18 && this.nsfw) || !data.over_18) &&
        !data.quarantine &&
        this.domains.includes(data.domain)
      ) {
        this.ResponseUrl = data.url;
        result = true;
        break;
      }
    }

    return result;
  }
}

/**
 * A model of the response returned by Reddit, as given by their official API documentation.
 * Only the properties relevant to the code are represented here.
 */
interface IRedditRandomResponse {
  StatusCodeError: string;
  kind: string;
  data: {
    modhash: string;
    dist: number;
    children: [
      {
        kind: string;
        data: {
          hidden: boolean;
          quarantine: boolean;
          subreddit_type: string;
          domain: string;
          is_self: boolean;
          over_18: false;
          url: string;
        };
      }
    ];
  };
}

/**
 * A model of the response given by Reddit when querying the ABOUT page, only the relevant bits are represented here.
 */
interface IRedditAboutResponse {
  data: {
    quarantine: boolean;
    over18: false;
  };
}

/**
 * A model of the error response returned by Reddit when querying a sub that is quarantined or banned etc.
 */
interface IRedditErrorResponse {
  reason: string;
  message: string;
  error: number;
}
