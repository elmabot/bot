import * as fs from "fs";
import * as Path from "path";
import { Command } from "../../base";
import { BASEDIR } from "../../bot";

export class Bisu extends Command {
  protected commandName = "bisu";
  protected desc = "Bisu";
  protected needParamValues = false;

  constructor() {
    super();
  }

  public exec(): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      if (fs.existsSync(Path.join(BASEDIR, "/res/img/bisu.png"))) {
        this.reply = {
          files: [
            {
              attachment: Path.join(BASEDIR, "/res/img/bisu.png"),
              name: "Bisu.png",
            },
          ],
        };
        resolve(true);
      } else {
        this.errorString = "Bisu image not found";
        this.error = new Error("Bisu image not found");
        reject(false);
      }
    });
  }
}
