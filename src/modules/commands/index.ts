import { Admin } from "./admin";
import { Bisu } from "./bisu";
// import {Booru} from "./booru";
import { Choose } from "./choose";
import { Eightball } from "./eightball";
import { Feedback } from "./feedback";
import { Help } from "./help";
import { How } from "./how";
// import { Markov } from "./markov";
import { Reddit } from "./reddit";
import { Sway } from "./sway";
import { Swift } from "./swift";
import { Thanos } from "./thanos";

export const commandList = [
  Admin,
  Bisu,
  Choose,
  Eightball,
  Feedback,
  How,
  Help,
  Reddit,
  Sway,
  Swift,
  Thanos,
];
