import { Command, config } from "../../base";

export class How extends Command {
  protected desc = "tells you much X is Y in percentages";

  constructor() {
    super();
    this.commandName = "how";
    this.params = "<what much something is> is <the something in question>";
  }

  public exec(): Promise<boolean | Error> {
    return new Promise((resolve, reject) => {
      try {
        const regex = new RegExp(
          "^" +
            config.global.prefix +
            this.commandName +
            " (.+) (is|are) (.+[^\\?])\\??",
          "mi"
        );
        const theMatch = this.message.content.match(regex);

        if (theMatch === null) {
          this.reply =
            "There seems to be a problem, are you sure you wrote the command correctly?";
          resolve(true);
        } else {
          // picks a random number and just mulitplies it by 100 to get a weird percentage based thing
          const randomNumber = Math.random() * 100;
          const num = randomNumber.toFixed(2);
          this.reply =
            theMatch[3] + " " + theMatch[2] + " " + num + "% " + theMatch[1];
          resolve(true);
        }
      } catch (e) {
        this.error = e;
        reject(e);
      }
    });
  }
}
