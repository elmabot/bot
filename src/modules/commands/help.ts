import { Command, CommandCaller, ICaller } from "../../base";
import { commands } from "../../bot";
import { getPrefix, theNick } from "../../util";
import { commandList } from "./index";

export class Help extends Command {
  protected desc = "Gives a list of commands or general information about them";

  private readonly commands: Record<string, unknown>;
  private commandClasses = commandList;

  constructor() {
    super();
    this.aliases = "list";
    this.commandName = "help";
    this.params = "<command>";
    this.commands = commands.getList();
    this.needParamValues = false;
  }

  public exec(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      if (this.paramValues.length >= 1) {
        this.reply = this.echoCommand(this.paramValues[0]);
        resolve(true);
      } else if (this.paramValues.length < 1) {
        this.reply = this.echoList();
        resolve(true);
      } else {
        this.errorString = "params incorrectly given or NaN";
        reject(false);
      }
    });
  }

  /**
   * Outputs a rich embed reply with the specifics of a command, or a negative confirmation if the reply doesn't exist
   * @param command - the name of the command to look for
   * @returns {string|Object}
   */
  public echoCommand(command: string): Record<string, unknown> | string {
    try {
      command = command.toLowerCase();
      let reply: Record<string, unknown> | string;
      const arg = this.commands[command] as ICaller;
      if (arg !== undefined) {
        const commCall = new CommandCaller(arg);
        const comm = commCall.getCommand() as Command;
        const commProps = comm.returnProperties();
        reply = {
          embed: {
            color: 53971,
            description: commProps.desc,
            fields: [
              {
                name: "Syntax",
                value: commProps.syntax,
              },
            ],
            footer: {
              icon_url: this.message.member.user.avatarURL,
              text: "Requested by " + theNick(this.member),
            },
            title: commProps.name,
          },
        };
      } else {
        reply = "I don't recognize that command";
      }
      return reply;
    } catch (e) {
      this.error = e;
    }
  }

  /**
   * Outputs a list of available commands and aliases as a string
   * @returns {string}
   */
  protected echoList(): string {
    let list =
      "These are all currently available commands and their aliases:\n```\n";
    for (const command of commandList) {
      let toAdd = "";
      const comm = new command();
      const commProp = comm.returnProperties();
      if (commProp.alias !== undefined) {
        toAdd = " - ";
        if (Array.isArray(commProp.alias)) {
          for (const ali of commProp.alias) {
            toAdd += ali + ", ";
          }
          toAdd = toAdd.substr(0, toAdd.length - 2);
        } else {
          toAdd += commProp.alias;
        }
      }
      list += commProp.name + toAdd + "\n";
    }
    list +=
      "```use " +
      getPrefix(this.server) +
      "help with a specific command to get more information";
    return list;
  }
}
