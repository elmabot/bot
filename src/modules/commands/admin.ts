import { Command, config, db } from "../../base";

export class Admin extends Command {
  protected commandName = "admin";
  protected desc =
    "A special administration command to configure a bot's settings";
  protected params = "<parameter> <value>";
  protected aliases = "config, administrate";
  protected replyTo = true;

  private readonly functions: string[];

  constructor() {
    super();
    this.functions = [];
  }

  public exec(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      if (this.message.author.id === config.discord.ownerid) {
        const funcs = this.functions;
        try {
          const regexMatch = this.message.content.match(
            new RegExp("^(\\S+) (\\w+) (\\S+)?( .*)?", "mi")
          );
          if (Array.isArray(regexMatch) && regexMatch.length > 2) {
            regexMatch[2] = regexMatch[2].toLowerCase();
            regexMatch[3] = regexMatch[3].toLowerCase();
            if (funcs.includes(regexMatch[2])) {
              // eslint-disable-next-line no-empty
              switch (regexMatch[2]) {
              }
            } else {
              this.reply = "I'm sorry that admin command doesn't seem to exist";
              resolve(true);
            }
          } else {
            this.reply =
              "I'm sorry I've encountered an error trying to read your command";
            resolve(true);
          }
        } catch (e) {
          this.reply =
            "I'm sorry I've encountered an error trying to read your command";
          this.error = e;
          reject(false);
        }
      } else {
        this.reply = "you are not authorised to use this command";
        resolve(true);
      }
    });
  }
}
