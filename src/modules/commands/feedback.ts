import { TextChannel } from "discord.js";
import { Command, config } from "../../base";
import { theNick } from "../../util";

export class Feedback extends Command {
  protected commandName = "feedback";
  protected desc = "Send a feedback message to the bot owner";

  constructor() {
    super();
  }

  protected exec(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      if (!config.discord.feedbackchan) {
        this.reply =
          "No feedback channel is set, this message won't go anywhere :(";
        this.error = new Error("No feedback channel set in config file");
        resolve(false);
      }
      const feedbackChan: TextChannel = this.getFeedbackChan();
      const feedbackmessage: string = this.paramValues.join(" ");

      let thumbnail: string;
      let originString: string;
      const author = this.message.author;
      if (!this.message.guild) {
        originString = `${author.tag} said in a PM`;
        thumbnail = author.defaultAvatarURL;
      } else {
        const name = theNick(this.member);
        originString = `${name} said in ${this.message.guild.name}`;
        thumbnail = this.message.guild.iconURL();
      }

      const feedback: Record<string, unknown> = {
        embed: {
          author: {
            icon_url: author.avatarURL,
            name: `Sent by ${author.tag}`,
          },
          color: 10376700,
          description: feedbackmessage,
          fields: [
            {
              name: "sender ID",
              value: author.id,
            },
          ],
          thumbnail: {
            url: thumbnail,
          },
          timestamp: new Date().toLocaleTimeString(),
          title: originString,
        },
      };
      feedbackChan
        .send(feedback)
        .then(() => {
          this.reply = "Message has been sent ^^";
          resolve(true);
        })
        .catch((err) => {
          this.error = new Error(err);
          reject(false);
        });
    });
  }

  private getFeedbackChan(): TextChannel {
    return this.message.client.channels.cache.get(
      config.discord.feedbackchan
    ) as TextChannel;
  }
}
