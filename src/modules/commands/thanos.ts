import * as fs from "fs";
import * as Path from "path";
import { Command } from "../../base";
import { BASEDIR } from "../../bot";

export class Thanos extends Command {
  protected commandName = "thanos";
  protected desc = "Summons the Titan to grace your Discord server";
  protected needParamValues = false;

  constructor() {
    super();
  }

  public exec(): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      if (fs.existsSync(Path.join(BASEDIR, "/res/img/thanos.gif"))) {
        this.reply = {
          files: [
            {
              attachment: Path.join(BASEDIR, "/res/img/thanos.gif"),
              name: "thanos.gif",
            },
          ],
        };
        resolve(true);
      } else {
        this.errorString = "Thanos image not found";
        this.error = new Error("Thanos image not found");
        reject(false);
      }
    });
  }
}
