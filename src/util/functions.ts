import { GuildMember } from "discord.js";
import { config } from "../base";
import { settings } from "../bot";

/**
 * checks whether or not a discord user has a nickname or not
 * if not, returns the user's discord username
 * @param member - a discordjs member object
 * @returns {string} - either the nickname or username
 */
export function theNick(member: GuildMember): string {
  if (member.nickname === null) {
    return member.user.username;
  } else {
    return member.nickname;
  }
}

/**
 * Small utility function to get a server's command prefix, or default to the global one if none exists
 * @param server - the id as a string
 * @returns {string} - the prefix
 */
export function getPrefix(server: string): string {
  if (settings.general[server].commandprefix === undefined) {
    return config.global.prefix;
  } else {
    return settings.general[server].commandprefix;
  }
}

/**
 * Returns the prefix but as a usable RegExp object, calls the getPrefix function internally
 * @param server - the id as a string
 * @returns {RegExp} - the RegExp object with the prefix of the server
 */
export function getPrefixRegex(server: string): RegExp {
  return new RegExp("^" + getPrefix(server) + "(\\w+) ?(.*)?", "mi");
}
