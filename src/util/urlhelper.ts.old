import validator from "validator";
import { config, logger } from "../base";
import { VERSION } from "../bot";

export class Urlhelper {
  public static getUrls(input: string) {
    const matches: RegExpMatchArray | null = input.match(
      new RegExp("https?:\\/\\/\\S+", "gi")
    );
    if (matches !== null) {
      return matches;
    } else {
      return false;
    }
  }

  public static checkIfSafe(input: string) {
    if (validator.isEmpty(config.google.safebrowse) === false) {
      const uri = `https://safebrowsing.googleapis.com/v4/threatMatches:find?key={$config.google.safebrowse}`;

      const opt = {
        body: {
          client: {
            clientId: "elmabot",
            clientVersion: VERSION,
          },
        },
        headers: {
          "Content-Type": "application/json",
        },
        json: true,
        method: "POST",
        uri,
      };
      return true;
    } else {
      logger.warn("safebrowse API could not be queried, API key not specified");
      return false;
    }
  }

  public urls: RegExpMatchArray | boolean;
  private isSafe: boolean;

  constructor(input: string) {
    this.isSafe = true;
    this.urls = Urlhelper.getUrls(input);
  }
}
