import fetch, { Response } from "node-fetch";
import { logger } from "../base/config";

export class HTTPResponseError extends Error {
  private response: Response;

  constructor(response: Response, msg = "") {
    let text = `HTTP Error Response: ${response.status} ${response.statusText}`;
    if (msg !== "") {
      text = `${msg} \n ${text}`;
    }
    super(text);
    this.response = response;
  }
}

function checkStatus(response: Response) {
  if (response.ok !== undefined && response.ok) {
    return response;
  } else {
    throw new HTTPResponseError(response);
  }
}

export async function fetchToJSON(
  url: string,
  options: Record<string, unknown>
): Promise<unknown> {
  let resp = await fetch(url, options).catch((reason) => {
    logger.error(reason);
    return false;
  });
  if (!resp) {
    return false;
  }
  resp = resp as Response;
  try {
    checkStatus(resp);
  } catch (err) {
    logger.error(`${err.name} ${err.message} \n ${err.stack}`);
  }
  const json = await resp.json().catch((reason) => {
    logger.error(reason);
    return false;
  });
  return json;
}
