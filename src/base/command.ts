import { GuildMember, Message } from "discord.js";
import { getPrefixRegex } from "../util";
import { logger } from "./config";

export interface ICaller {
  new (): Command;
}

export class CommandCaller {
  constructor(private call: ICaller) {}

  getCommand(): Command {
    return new this.call();
  }
}

/**
 * A generic, abstract command class from which all other commands are going to derive
 */
export abstract class Command {
  public error: Error;
  public errorString: string;
  public warning: Error;

  protected commandName: string;
  protected aliases: string | string[];
  protected abstract desc: string;
  protected params: string;
  protected server: string;
  protected replyTo = false;
  protected reply: string | Record<string, unknown>;
  protected message: Message;
  protected member: GuildMember;
  protected needParamValues = true;
  protected paramValues: string[];

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  constructor() {}

  /**
   * Returns the commands properties, for use in the Help command
   * @returns object
   */
  public returnProperties(): {
    name: string;
    alias: string | string[];
    desc: string;
    syntax: string;
  } {
    return {
      alias: this.aliases,
      desc: this.desc,
      name: this.commandName,
      syntax: this.returnSyntax(),
    };
  }

  /**
   * The meat of the object, this function is called by the commander object and instantiates
   * the object proper with all required values, then runs the encompassing function and
   * returns the resulting promise object
   * @param message
   * @return {Promise}
   */
  public run(message: Message): Promise<unknown> {
    this.message = message;
    this.member = message.member;
    this.server = this.message.guild.id;
    this.paramValues = this.getParams();
    if (this.paramValues.length < 1 && this.needParamValues === true) {
      this.reply =
        "this command requires parameters, please use the help function if you are unsure";
      return new Promise((resolve) => resolve(true));
    } else {
      return this.exec();
    }
  }

  /**
   * A base function that returns whatever reply has been set by other base functions,
   * the command object returns this response after the command has been successfully ran
   * defaults to sending it as a normal message, but can send it as reply if replyTo is set to true
   */
  public respond(): void {
    if (this.replyTo === true) {
      this.message.reply(this.reply).catch((err) => {
        logger.error(err.stack);
      });
    } else {
      this.message.channel.send(this.reply).catch((err) => {
        logger.error(err.stack);
      });
    }
  }

  /**
   * Wrapper function which sets a generic error message to respond with and calls `respond()`
   */
  public errorRespond(): void {
    this.reply = "I'm sorry but it seems like an error has occurred";
    this.respond();
  }

  /**
   * Gets an an array with all the parameters passed to a command
   * @returns string[]
   */
  protected getParams(): string[] {
    const result: RegExpMatchArray | null = this.message.content.match(
      getPrefixRegex(this.message.guild.id)
    );
    if (result !== null && result[2] !== undefined) {
      return result[2].split(" ");
    } else {
      return [];
    }
  }

  /**
   * a base run function to be expanded upon per command,
   * Each command should perform it's main functions within this umbrella function
   * @returns {Promise}
   */
  protected abstract exec(): Promise<unknown>;

  /**
   * Returns the syntax of the command
   * @returns string
   */
  private returnSyntax(): string {
    if (this.params === undefined) {
      return this.commandName;
    } else {
      return this.commandName + " " + this.params;
    }
  }
}
