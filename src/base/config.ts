// eslint-disable-next-line @typescript-eslint/no-var-requires
require("dotenv-safe").config();

import * as fs from "fs";
import { TransformableInfo } from "logform";
import * as sqlite3 from "sqlite3";
import * as toml from "toml";
import { createLogger, format, transports } from "winston";

const sqlite = sqlite3.verbose();
const { combine, timestamp, printf } = format;
const myFormat = printf((info: TransformableInfo) => {
  return `${info.timestamp} ${info.level}: ${info.message}`;
});

const ENV = process.env;

// eslint-disable-next-line @typescript-eslint/no-var-requires
const newLocal = require("../../package.json").version;
export const VERSION = newLocal;

// set up the logger using Winston
export const logger = createLogger({
  format: combine(timestamp(), myFormat),
  transports: [
    new transports.File({ filename: "log/error.log", level: "error" }),
    new transports.File({ filename: "log/combined.log" }),
  ],
});

if (ENV.NODE_ENV == "development") {
  logger.add(
    new transports.Console({
      stderrLevels: ["error"],
      consoleWarnLevels: ["warn", "debug"],
      level: "info",
    })
  );
}

/**
 * simple function to get and parse the TOML config file
 * @returns {array}
 */
function getConfig(): Config {
  try {
    return toml.parse(fs.readFileSync("./config/config.toml", "utf8"));
  } catch (e) {
    logger.error(
      "Error parsing config file on line " +
        e.line +
        ", columnn " +
        e.column +
        ": " +
        e.message
    );
    process.exit(1);
  }
}

/**
 * Describes the structure of the config file
 */
export interface Config {
  global: {
    prefix: string;
    name: string;
  };
  discord: {
    token: string;
    ownerid: string;
    feedbackchan: string;
  };
  google: {
    safebrowse: string;
  };
}

export const config: Config = getConfig();
// set up database, needs actual database to exist or else bot gets unhappy
export const db = new sqlite.Database("./config/db.sqlite");
