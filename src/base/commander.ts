import { Message } from "discord.js";
import { commandList } from "../modules/commands";
import { Command, CommandCaller, ICaller } from "./command";
import { logger } from "./config";

/**
 * The Commander singleton handles the calling of and treatment of call command objects, nominally only one of these
 * should exist at any given moment.
 */
export class Commander {
  /**
   * A list of references to command classes exported by index.ts in the commands directory
   */
  private readonly commandList = commandList;
  /**
   * This final list will be used to call all commands with their names and aliases
   */
  private callList: Record<string, unknown>;
  /**
   * This object will be temporarily used to store the names of commands, as opposed to aliases
   */
  private nameList: Record<string, unknown> = {};
  /**
   * This object will be temporarily used to store the aliases of commands
   */
  private aliasList: Record<string, unknown> = {};

  /**
   * This wrapper function will be called to run the actual command after a prefix has been detected
   * This handles both the calling of the command, returning it's reply in chat, and handling it's errors
   * @param command - the name of the command to be called
   * @param message - the discordjs Message object of the message in which the command was called
   */
  public run(command: string, message: Message): void {
    command = command.toLowerCase();
    const list = this.callList;
    const arg = list[command] as ICaller;
    if (!arg) {
      message.channel
        .send(
          "I don't think I have a command with that name? You can use the 'help' command to " +
            "list all available commands"
        )
        .then()
        .catch((err: Error) => logger.error(err.stack));
    } else {
      const commCall = new CommandCaller(arg);
      const comm = commCall.getCommand() as Command;
      comm
        .run(message)
        .then((response: boolean) => {
          if (!response) {
            this.handleWarning(comm);
          }
          comm.respond();
        })
        .catch(() => {
          this.handleError(comm);
          comm.errorRespond();
        });
    }
  }

  /**
   * A simple function to register all command names and aliases, unfortunately this function has to be called after
   * @remark object instantiation as it requires the Settings object to be instantiated
   * @return boolean - true if successful, false if an error occurs
   */
  public register(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.createLists()
        .then(() => {
          const nameList = this.nameList;
          const aliasList = this.aliasList;
          this.callList = Object.assign(nameList, aliasList);
          resolve(true);
        })
        .catch((err: Error) => {
          logger.warn(
            "Error occurred while building the command list: [" +
              err.name +
              "]--" +
              err.message
          );
          reject(false);
        });
    });
  }

  /**
   * Returns the list of commands and aliases, mostly for the [[Help]] command
   * @return object - the list of commands and aliases
   */
  public getList(): Record<string, unknown> {
    return this.callList;
  }

  /**
   * A separate function to handle warnings a command might emit
   * @param comm - the command object
   */
  private handleWarning(comm: Command): void {
    const commName = comm.returnProperties().name.toUpperCase();
    const warn = comm.warning;
    if (comm.errorString.length > 2) {
      logger.warn(
        comm.errorString +
          "\n[" +
          commName +
          "] -- " +
          warn.name +
          "-" +
          warn.stack +
          "\n##" +
          warn.message
      );
    } else {
      logger.warn(
        "[" + commName + "] -- " + warn.name + "\n ##" + warn.message
      );
    }
  }

  /**
   * A function to handle errors a command might return
   * @param comm - the command object
   */
  private handleError(comm: Command): void {
    const commName = comm.returnProperties().name.toUpperCase();
    const error = comm.error;
    if (comm.errorString !== undefined && comm.errorString.length > 2) {
      logger.error(
        `${comm.errorString}
 ##[${commName}] -- ${error.name}:${error.message}
 ###${error.stack}`
      );
    } else {
      if (error !== undefined) {
        logger.error(
          `
 ##[${commName}] -- ${error.name}:${error.message}
 ###${error.stack}`
        );
      } else {
        logger.error(`UNKOWN ERROR OBJECT RETURNED: ${error}`);
      }
    }
  }

  /**
   * This function takes care of setting up the actual lists of command names and aliases
   * The function instantiates all Classes found in the commandList property one by one to return their properties
   * @return Promise - returns true when all lists have been created, returns false if an error occurs
   */
  private createLists(): Promise<false> {
    return new Promise((resolve, reject) => {
      for (const command of this.commandList) {
        try {
          const comm = new command();
          const properties = comm.returnProperties();
          this.registerName(properties.name, command);
          if (Array.isArray(properties.alias)) {
            for (const ali of properties.alias) {
              this.registerAlias(ali, properties.name, command);
            }
          }
          if (typeof properties.alias === "string") {
            this.registerAlias(properties.alias, properties.name, command);
          }
        } catch (err) {
          reject(err);
        }
      }
      resolve(false);
    });
  }

  /**
   * Registers a Command Class by it's name property
   * @param Name
   * @param command
   */
  private registerName(Name: string, command: unknown): void {
    if (Name in this.nameList) {
      logger.error(
        "Duplicate command with the name " + Name + ", please resolve"
      );
      process.exit(2);
    } else {
      this.nameList[Name] = command;
    }
  }

  /**
   * Registers Command Classes with their aliases and checks for duplicates
   * If an alias already exists as a command name it will not be added
   * If an alias has been registered before it will not be registered
   * @param Alias
   * @param CommandName
   * @param command
   */
  private registerAlias(
    Alias: string,
    CommandName: string,
    command: unknown
  ): boolean {
    if (Alias in this.nameList) {
      logger.warn(
        "Alias [" +
          Alias.toUpperCase() +
          "] already exists as a command, will not be added"
      );
      return false;
    }
    if (Alias in this.aliasList) {
      logger.warn(
        "An alias for command " +
          CommandName.toUpperCase() +
          " already exists as" +
          Alias.toUpperCase() +
          ", will not be overridden"
      );
      return false;
    }
    this.aliasList[Alias] = command;
  }
}
