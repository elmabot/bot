/* TODO maybe rewrite this to store the settings in a database instead of in-memory */
import { Client, Guild, TextChannel } from "discord.js";
import { config, db, logger } from "./config";
import { discordClient } from "../bot";

/**
 * interface which mirrors the structure of settings stored in the database
 */
interface Isettings {
  [index: string]: {
    commandprefix: string;
    isnsfw: string;
    noconfig: string;
    tweetCheckChannel: string;
    tweetemote: string;
  };
}

/**
 * This object is meant to store the bot's settings on a per-server basis. The settings are permanently stored in SQL
 * format in a database and used as a base on every bot startup.
 */
export class Settings {
  /**
   * array of guild `Collection` objects as defined by Discord.js
   */
  public guilds: Guild[];
  /**
   * Stores the main settings which will get checked
   */
  public general: Isettings;
  /**
   * utility parameter which allows the `Settings` object to indicate whether or not it is ready to be used
   */
  public ready: boolean;

  /**
   * Stores the rights of users as a javascript array
   */
  public rights: Record<string, unknown>;
  /**
   * the Discord snowflake ID of the bot owner for override purposes
   */
  public ownerId: string;
  /**
   * stores the Discord snowflake IDs of all guilds the bot is in
   */
  public guildString: string;

  constructor(client: Client, owner: string) {
    this.guilds = client.guilds.cache.array();
    this.ownerId = owner;
    this.ready = false;
    this.general = {};
    this.rights = {};
    this.guildString = "";
  }

  /**
   * Gets a string of IDs of all guilds the bot is currently in
   */
  public getGuildString(): void {
    for (let i = 0; i < this.guilds.length; i++) {
      if (i === 0) {
        this.guildString = this.guildString + this.guilds[i].id;
      } else {
        this.guildString = this.guildString + ", " + this.guilds[i].id;
      }
    }
  }

  /**
   * creates a blank configuration template in the database for when a server has none
   * @param server - the discord snowflake ID of the server
   * @returns Promise<boolean|Error> - returns `true` when the blank config was successfully created or an error
   * object when an error occurs.
   */
  public createBlankConfig(server: string): Promise<boolean | Error> {
    return new Promise((resolve, reject) => {
      db.run(
        "INSERT INTO serverSettings (serverid) VALUES (?)",
        [server],
        (re: Error) => {
          if (re === null) {
            resolve(true);
          } else {
            reject(re);
          }
        }
      );
    });
  }

  /**
   * returns a promise with the settings of all servers for which configurations exist within the database
   * @returns Promise<Object|Error> - returns an array with all settings per server or an error object if an error
   * occurred and no settings can be retrieved
   */
  public getSettings(): Promise<Isettings | Error> {
    return new Promise((resolve, reject) => {
      const settings: Isettings = {};
      const sql =
        "SELECT * FROM `serverSettings` WHERE `serverid` IN (" +
        this.guildString +
        ");";
      db.each(
        sql,
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        (err: Error, row: any) => {
          if (err !== null) {
            logger.warn(
              "error while getting settings for server : " + JSON.stringify(err)
            );
          } else {
            settings[row.serverid] = row;
            if (
              row.commandprefix === null ||
              row.commandprefix === undefined ||
              row.commandprefix === 0
            ) {
              settings[row.serverid].commandprefix = config.global.prefix;
            }
          }
        },
        (finErr: Error) => {
          if (finErr !== null) {
            reject(finErr);
          } else {
            resolve(settings);
          }
        }
      );
    });
  }

  /**
   * Returns all rights which have been stored in the database
   * @returns Promise<{}|Error> Returns the rights object or an error object if an error occurs
   */
  public getRights(): Promise<Record<string, unknown> | Error> {
    const rights: Record<string, unknown> = {};
    return new Promise((resolve, reject) => {
      db.each(
        "SELECT * FROM `rights` WHERE `server` IN (?);",
        [this.guildString],
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        (err: Error, row: any) => {
          if (err !== null) {
            logger.warn(
              "error while getting rights for server : " + JSON.stringify(err)
            );
          } else {
            rights[row.serverid] = row;
          }
        },
        (finErr: Error) => {
          if (finErr !== null) {
            reject(finErr);
          } else {
            resolve(rights);
          }
        }
      );
    });
  }

  /**
   * wrapper function which will retrieve all rights and settings for all guilds and stores them in the object.
   * This function is asynchronous in order to allow the bot's setup to be delayed in order to wait for the settings
   * to be initialized.
   * @returns Promise<boolean> - returns true if the initialisation succeeded or false if it failed.
   */
  public init(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      const guilds = this.guilds;
      if (guilds.length < 1) {
        reject(false);
      }

      this.getGuildString();

      this.getSettings()
        .then((settings: Isettings) => {
          let secIndex = 0;
          const guildSett = Object.keys(settings);
          for (const guild of guilds) {
            const guildId = guild.id;

            if (guildSett.includes(guildId)) {
              secIndex++;
            } else {
              this.createBlankConfig(guildId)
                .then(async () => {
                  const targetChan = (await discordClient.channels.fetch(
                    guild.systemChannelID
                  )) as TextChannel;
                  targetChan.send(
                    "No configuration exists for this server, use the " +
                      config.global.prefix +
                      "config command to set them"
                  );
                  secIndex++;
                })
                .catch(async (re) => {
                  logger.error(
                    "Error occurred when trying to create blank " +
                      "configuration for server : " +
                      JSON.stringify(re)
                  );
                  const targetChan = (await discordClient.channels.fetch(
                    guild.systemChannelID
                  )) as TextChannel;
                  targetChan.send(
                    "No configuration exists for " +
                      "this server and an error occurred while attempting to set one. " +
                      "Please contact the bot owner if you wish to configure this server"
                  );
                  secIndex++;
                });
            }
            if (secIndex === guilds.length) {
              this.general = settings;
            }
          }
          this.getRights()
            .then((rights: Record<string, unknown>) => {
              this.rights = rights;
              this.ready = true;
            })
            .catch((finErr: Error) => {
              logger.error(
                "error while querying server rights : " +
                  finErr.name +
                  ": " +
                  finErr.message
              );
            });
          resolve(true);
        })
        .catch((finErr: Error) => {
          logger.error(
            "error while querying server settings : " +
              finErr.name +
              ": " +
              finErr.message
          );
          resolve(false);
        });
      this.guilds = null;
      resolve(true);
    });
  }
}
