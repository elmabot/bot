import { VERSION } from "./base/config";

test("version", () => {
  expect(VERSION).toBe("0.5.9");
});
