<div align="center">

<h2>Elma, a Discord Bot</h2>
<img src="https://elmabot.gitlab.io/docs/img/elma_scruffyturtles.png" alt="Elma by Scruffyturtles">
<br><a href="https://twitter.com/ScruffyTurtles"><sub>Elma by Scruffyturtles </sub></a>
</div>

---

### What is Elma?

Elma is a custom Discord bot built to fulfill a couple of different tasks. Formerly exclusively for one server, but now
runs on other servers as well. Some of Elma's functionality is coded to be exclusive to some servers, most notably the
Twitter posting functions, but when running your own version you can open them up to public use.

### Your own version

This part of the guide assumes you are comfortable installing and operating Node.js software, either on a local machine
or on a remote server. The following part is written assuming the software will be run on Linux, specifically Debian.

[Instructions for the development version are down here.](#dev)

#### Installation

1. Fork this repository from [here](https://gitlab.com/elmabot/elmabot/forks/new)
2. Clone your fork to wherever you intend to run it
3. Navigate inside the `Elma` directory inside your cloned repo
4. `npm install` all dependencies
5. set up your local `config.toml` file.

You can run `npm run setup` inside to rename the files automatically and compile the Javascript code.

Alternatively you can do this manually by editing
`config.example.toml` and `db.example.sqlite` by removing the `example` bit to have an empty template to work from.

Comments inside the TOML file will tell you what's needed, mainly you should make sure to set the token of your Discord Bot

**Note: the bot will not run if these files do not exist**

#### Running Elma

If you manually set up the files you'll need to transpile the typescript code to run it, this can be done using a simple npm
script, once this is done you can start it using the normal npm `start` command.  
Like this:

```shell script
npm run build
npm start
```

Or you can run it manually:

```shell script
rm -rf dist/bot.* dist/**/* && tsc -p tsconfig.json
node dist/bot.js
```

I recommend using a process manager to take care of the actual process, rather than having ir run continuously in CLI or
running it headless. Personally I favor [PM2](https://pm2.io/doc/en/runtime/overview/).  
An `ecosystem.config.js` file is included, all you need to do is to install PM2 and simply start it inside the `Elma` directory.

```shell script
npm install pm2 -g
pm2 start
```

This will spawn a process named `Elma`, errors and output will be logged to the `log` directory alongside application
logs. PM2 will watch for file changes and errors and restart Elma if necessary.

### <a name="dev"></a> Setting up a development version

_The repo comes with a [Vagrant](https://www.vagrantup.com/) configuration, including a separate provisioning script._

Otherwise, simply follow the previous instructions to test your bot locally, make sure your `NODE_ENV` variable is set to
`Development`. The Vagrant provisioning will do this automatically for your vagrant box:

```shell script
NODE_ENV=Development
npm install --save-dev
```

(This works on Debian and other Linux distributions, you may have to proceed differently depending on your choice of OS)

This project also supports the use of [Nodemon](https://github.com/remy/nodemon). It is included with the development dependencies
so simply running.

```shell script
npm run start-dev
```

Will already start it if everything is installed. You can also choose to use your own global install of Nodemon if you wish.  
Nodemon will watch for any changes to TypeScript files and restart your bot for testing purposes. This is done using
[ts-node](https://github.com/TypeStrong/ts-node) library.

#### Using Vagrant

The project includes an NPM script to start up the vagrant instance, running:

```shell script
npm run vagrant
````
Or...
```
vagrant up && vagrant rsync-auto
```

will start up a vagrant box and use rsync with watch function to synchronize files between your local copies. You will
need to let this run in your console of choice and start up a new console window/instance to start your development version inside the Vagrant box:

```shell script
vagrant ssh
```

Should immediately connect you in the Elma directory, allowing you to start the bot using the regular
startup commands.

### Generating docs

Running the `doc` command like so

```shell script
npm run doc
# alternatively you can run
typedoc --tsconfig tsconfig.json
```

Will will run a regular `build` command and, upon completion, generate documentation using [TypeDoc](https://typedoc.org/).
Currently no testing is integrated, but is planned for a future date.

---

### License

Elma by [Clerical Terrors](https://gitlab.com/clericalterrors) is licensed under a [GNU Affero General Public License](LICENSE.md)
