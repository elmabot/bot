FROM node:slim

ENV NODE_ENV=production

RUN mkdir -p /home/node/Elma && chown -R node:node /home/node/Elma

WORKDIR /home/node/Elma

RUN npm install -g npm@latest

RUN npm install -g pm2

COPY --chown=node:node package*.json ./

USER node

RUN npm ci --production

COPY --chown=node:node . .

RUN npm run build

VOLUME ./config:/home/node/Elma/config/
VOLUME ./log/:/home/node/Elma/log/
VOLUME ./res/markov:/home/node/Elma/res/markov/

CMD [ "pm2-runtime", "start", "ecosystem.config.js" ]

